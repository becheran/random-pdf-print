import os
import random
import subprocess
import glob
from time import sleep

PDF_FOLDER = 'content'
PRINTER = 'EPSON855530 (Epson Stylus Photo R3000)'
DELAY_SECONDS = 120


def main():
    while True:
        pdfs = find_pdfs()
        random.shuffle(pdfs)
        for pdf in pdfs:
            print('Print PDF "{}"'.format(pdf))
            # See: https://www.doc2prn.com/syntax
            exit_code = subprocess.run('PDFtoPrinter.exe {} \"{}\"'.format(pdf.replace('\\', '/'), PRINTER))
            print('Exit code: {}'.format(exit_code))
            sleep(DELAY_SECONDS)    
        print('Printed all PDFs. Restart from beginning.')


def find_pdfs():
    all_pdfs = list()
    for pdf in glob.glob(os.path.join(PDF_FOLDER, "*.pdf")):
        all_pdfs.append(pdf)
    return all_pdfs


if __name__ == "__main__":
    main()
